# https://hub.docker.com/r/microsoft/dotnet
FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build

# Copy csproj and restore as distinct layers.
WORKDIR /app
COPY WebPhonebook.sln .
COPY WebPhonebook/WebPhonebook.csproj WebPhonebook/
COPY PhonebookLib/PhonebookLib.csproj PhonebookLib/
COPY PhonebookLibTest/PhonebookLibTest.csproj PhonebookLibTest/
RUN dotnet restore

# Copy local code to the container image.
COPY . .

# Build a release artifact.
RUN dotnet publish -c Release -o /out WebPhonebook.sln

# Runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:2.2
ENV SOURCE=file

WORKDIR /app
COPY --from=build /out ./

# Run the web service on container startup.
CMD ["dotnet", "WebPhonebook.dll"]
