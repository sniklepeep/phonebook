﻿using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;


namespace PhonebookLib
{
    public class FirebasePhonebook : IPhonebook
    {
        #region Fields

        private FirestoreDb mDb;

        private CollectionReference mPersons;

        #endregion


        #region Init and clean-up

        public FirebasePhonebook(FirebaseSettings settings)
        {
            Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", settings.CredentialsPath);
            mDb = FirestoreDb.Create(settings.ProjectName);
            mPersons = mDb.Collection("persons");
        }

        #endregion


        #region IPhonebook implementation

        /// <inheritdoc/>
        void IPhonebook.Add(PhonebookEntry entry)
        {
            AddAsync(entry).GetAwaiter().GetResult();
        }


        /// <inheritdoc/>
        void IPhonebook.Delete(int id)
        {
            DeleteAsync(id).GetAwaiter().GetResult();
        }


        /// <inheritdoc/>
        ReadOnlyCollection<PhonebookEntry> IPhonebook.GetList(string filter)
        {
            var list = new List<PhonebookEntry>();

            var queryRes = mPersons.OrderBy("Id").GetSnapshotAsync().GetAwaiter().GetResult();

            foreach (var doc in queryRes.Documents)
            {
                var entry = new PhonebookEntry
                {
                    Id = doc.GetValue<int>("Id"),
                    FirstName = doc.GetValue<string>("FirstName"),
                    LastName = doc.GetValue<string>("LastName"),
                    BirthYear = doc.GetValue<int>("BirthYear")
                };

                // The filters in FB API are too weak
                if (entry.IsMatch(filter))
                {
                    list.Add(entry);
                }
            }

            return list.AsReadOnly();
        }

        #endregion


        #region Utility

        private async Task AddAsync(PhonebookEntry entry)
        {
            var idQuery = await mPersons.Select("Id").GetSnapshotAsync();
            entry.Id = idQuery.Count == 0 ? 1 : idQuery.Documents.Max(e => e.GetValue<int>("Id")) + 1;

            await mPersons.AddAsync(new
            {
                entry.Id,
                entry.FirstName,
                entry.LastName,
                entry.BirthYear
            });
        }


        private async Task DeleteAsync(int id)
        {
            var entry = await mPersons.WhereEqualTo("Id", id).GetSnapshotAsync();
            await entry.Documents.SingleOrDefault()?.Reference.DeleteAsync();
        }

        #endregion
    }
}
