﻿namespace PhonebookLib
{
    public class FileSettings
    {
        #region Constants

        public static readonly string DefaultPath =
            System.IO.Path.DirectorySeparatorChar == '/'
            ? "/app/pb.xml"
            : @"C:\Temp\pb.xml";

        #endregion


        #region Properties

        public string Path { get; set; } = DefaultPath;

        #endregion
    }
}
