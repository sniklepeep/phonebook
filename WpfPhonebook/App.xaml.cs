﻿using System;
using System.Windows;


namespace WpfPhonebook
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static DatabaseSources mSource;


        private void Application_Startup(object sender, StartupEventArgs e)
        {
            var argIdx = 0;

            if (e.Args[argIdx] == "-s")
            {
                argIdx++;
                if (argIdx == e.Args.Length)
                    throw new ArgumentException("-s shall be followed by a source definition.");

                mSource = SetSource(e.Args[argIdx]);
            }
        }


        private static DatabaseSources SetSource(string definition)
        {
            switch (definition)
            {
                case "sql":
                    return DatabaseSources.Sql;

                case "fb":
                    return DatabaseSources.Firebase;

                case "file":
                    return DatabaseSources.File;

                default:
                    throw new ArgumentException($"Unknown source definition '{definition}'.");
            }
        }
    }
}
